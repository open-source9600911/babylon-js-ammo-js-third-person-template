// Declare Ammo.js physics engine global variable for TypeScript understanding
declare var Ammo: any;

// Import necessary Babylon.js modules
import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";
import {
    Engine,
    Scene,
    Vector3,
    PhysicsImpostor,
} from "@babylonjs/core";
import { AmmoJSPlugin } from "@babylonjs/core/Physics/Plugins/ammoJSPlugin";
import { Environment } from "./Environment/environment";
import { Player } from "./Player/player";
import { SoccerBall } from './Ball/soccerBall'; // Import the soccer ball setup



class App {
    private scene: Scene;
    private engine: Engine;
    
    constructor() {
        console.log("App constructor: Creating and appending the canvas element to the DOM...");

        const canvas = document.createElement("canvas");
        canvas.style.width = "100%";
        canvas.style.height = "100%";
        canvas.id = "gameCanvas";
        document.body.appendChild(canvas);

        console.log("App constructor: Initializing the Babylon.js engine and scene...");
        this.engine = new Engine(canvas, true);
        this.scene = new Scene(this.engine);

        console.log("App constructor: Initialization complete. Proceeding with async setup...");
        this.initializeAsync();
    }

    private async initializeAsync() {
        console.log("initializeAsync: Starting async initialization...");
    
        await this.initPhysics();
    
        // Instantiate the Environment class after initializing physics
        new Environment(this.scene);
    
        // Instantiate the Player class (Assuming you have a player class ready and imported)
        // Instantiate the Player class
        const playerInstance = new Player(this.scene); // Make sure parameters match your Player class constructor
    
        // Enable CCD for the player's mesh through the playerInstance
        playerInstance.enableCCD(0.002, 0.1);
    
        // After initializing physics and adding dynamic objects to the scene, enable CCD
        //this.enableCCDForDynamicObjects(0.02, 0.5);

        // After initializing physics and adding dynamic objects to the scene, enable CCD
        const soccerBallInstance = new SoccerBall(this.scene);

          // Enable CCD for the player's mesh through the playerInstance
          soccerBallInstance.enableCCD(0.002, 0.1);
        
    
        console.log("initializeAsync: Async initialization complete. Starting the render loop...");

        //Turn on Debug Layer
        this.scene.debugLayer.show({
            embedMode: true,
          
        });

        //Render Loop
        this.engine.runRenderLoop(() => {
            const animationRatio = this.scene.getAnimationRatio();
    
            // Update player state with deltaTime and animationRatio
            playerInstance.update(animationRatio);
            this.scene.render();
            this.scene.debugLayer.show
        });
    
        console.log("initializeAsync: Render loop started.");
    }

   
    private async initPhysics() {
        console.log("initPhysics: Loading Ammo.js...");
        await Ammo(); // Ensure Ammo.js is loaded and ready
        console.log(typeof Ammo !== "undefined" ? "initPhysics: Ammo.js is available after init." : "initPhysics: Ammo.js is not available after init.");
    
        console.log("initPhysics: Enabling physics in the scene with Ammo.js...");
        const gravityVector = new Vector3(0, -9.81, 0);
        const physicsPlugin = new AmmoJSPlugin(true, Ammo);
        this.scene.enablePhysics(gravityVector, physicsPlugin);
        console.log("initPhysics: Physics enabled.");
    }

    //Enabling CCD

    private enableCCDForDynamicObjects(ccdMotionThreshold: number, ccdSweptSphereRadius: number): void {
    this.scene.meshes.forEach(mesh => {
        if (mesh.physicsImpostor) { // Apply CCD settings to all physics impostors for testing
            this.enableCCDForImpostor(mesh.physicsImpostor, ccdMotionThreshold, ccdSweptSphereRadius);
        }
    });
    }
    private enableCCDForImpostor(impostor: PhysicsImpostor, ccdMotionThreshold: number, ccdSweptSphereRadius: number): void {
        const ammoBody: any = impostor.physicsBody;
        if (window.Ammo && ammoBody) {
            ammoBody.setCcdMotionThreshold(ccdMotionThreshold);
            ammoBody.setCcdSweptSphereRadius(ccdSweptSphereRadius);
        } else {
            console.warn("CCD setup failed: Ammo.js is not available or physicsBody is missing.");
        }
    }
}

new App();
