import {
    Scene,
    MeshBuilder,
    Vector3,
    PhysicsImpostor,
    StandardMaterial,
    Color3,
    Mesh,
    ArcRotateCamera,
    Quaternion,
    Texture
} from "@babylonjs/core";
import { InputControls } from "./inputControls";
import { PlayerStaminaBar} from "./playerUI";


export class Player {
    public mesh: Mesh;
    private scene: Scene;
    private inputControls: InputControls;
    public camera: ArcRotateCamera;
   
   
    private stamina: number = 500; // Starting stamina
    private maxStamina: number = 500; // Maximum stamina
    private staminaRegenRate: number = 2.5; // Stamina regenerated per frame, adjust as needed
    private sprintStaminaConsumptionRate: number = 1; // Stamina consumed per frame while sprinting

   


    //UI elements
    private staminaBar: PlayerStaminaBar;
   

    constructor(scene: Scene) {
        this.scene = scene;
        this.inputControls = new InputControls(this.scene); // Initialize input controls
        this.staminaBar = new PlayerStaminaBar(); // Initialize the stamina bar UI
      

        this.createPlayerMesh();
        this.createCamera();
    }

    private createPlayerMesh() {
       
        this.mesh = MeshBuilder.CreateSphere("player", { diameter: 2 }, this.scene);
        this.mesh.position = new Vector3(0, 1, 0); // Adjusted to start slightly above the ground to see the effect of gravity
        
       // Assuming this.mesh has been created as your player's sphere
        const material = new StandardMaterial("coolMaterial", this.scene);

        // Apply a texture
        // Ensure you have a suitable texture available in your project's assets
        material.diffuseTexture = new Texture("textures/coolTexture.png", this.scene);

        // Add some specular highlights to make it 'shine'
        material.specularColor = new Color3(1, 1, 1); // Bright white specular highlights

        // Make the material a bit shiny
        material.specularPower = 64; // Adjust the power for more or less shininess

        // Apply the material to the mesh
        this.mesh.material = material;
    
        // Attach a physics impostor to enable physics
        this.mesh.physicsImpostor = new PhysicsImpostor(this.mesh, PhysicsImpostor.MeshImpostor, { mass: 4, restitution: 0, friction: 0 }, this.scene);
    
        // Immediately after creating the physics impostor, set angular factor to zero to prevent rotation
        if (this.mesh.physicsImpostor.physicsBody) {
            const physicsBody: any = this.mesh.physicsImpostor.physicsBody;

            // Check if the physics plugin is Ammo.js, as this operation is specific to Ammo.js
            if (this.scene.getPhysicsEngine()?.getPhysicsPlugin().name === "AmmoJSPlugin") {
                const zeroVector = new Ammo.btVector3(0, 0, 0);
                physicsBody.setAngularFactor(zeroVector);
            }
        }
    
        console.log("Player physics impostor created:", this.mesh.physicsImpostor);
    
        if (this.mesh.physicsImpostor) {
            console.log("Player physics impostor is applied.");
            // Optionally, check for specific properties
            console.log("Impostor mass:", this.mesh.physicsImpostor.mass);
        } else {
            console.log("Player physics impostor is NOT applied.");
        }
        
    }
    
    private lastLogTime: number = 0;
            
    //The update method calls all other other methods.
    public update(animationRatio: number) {
           
        const currentTime = performance.now();
        const inputVector = this.inputControls.getInputVector();
        const isSprinting = this.inputControls.isSprinting();


        // Log approximately every 1000 milliseconds
            if (currentTime - this.lastLogTime > 300) {
            console.log(`Current Stamina: ${this.stamina}`);
            console.log(`Stamina Regen Rate: ${this.staminaRegenRate}`);
            console.log(`Stamina Consumption Rate: ${this.sprintStaminaConsumptionRate}`);
            console.log(`Animation Ratio: ${animationRatio}`);
            this.lastLogTime = currentTime; // Update the last log time
            }

            // Call the move method with all required arguments
            this.move(inputVector, isSprinting, animationRatio);


        if (isSprinting && this.stamina > 0) {
        this.stamina -= this.sprintStaminaConsumptionRate * animationRatio;
        this.stamina = Math.max(this.stamina, 0); // Ensure stamina doesn't drop below 0
        }

        // Regenerate stamina if not sprinting and stamina is below maximum
        if (!isSprinting && this.stamina < this.maxStamina) {
        this.stamina += this.staminaRegenRate * animationRatio;
        this.stamina = Math.min(this.stamina, this.maxStamina); // Ensure stamina doesn't exceed maximum
        }

        // Update the UI to reflect the current stamina
        this.staminaBar.update(this.stamina / this.maxStamina);

    }

    public move(inputVector: Vector3, isSprinting: boolean, animationRatio: number) {
            this.alignMeshRotationWithCamera();
        
            let forward = this.camera.getForwardRay().direction;
            forward.y = 0; // Ensure horizontal movement only
            forward.normalize();
            const right = Vector3.Cross(Vector3.Up(), forward).normalize();
        
            let moveDirection = forward.scale(inputVector.z).add(right.scale(inputVector.x));
            if (!moveDirection.equals(Vector3.Zero())) {
                moveDirection = moveDirection.normalize();
            }
        
            // Apply animation ratio directly to base speed for frame rate independence
            const baseSpeed = 5; // Base speed adjusted by animation ratio
            const sprintMultiplier = isSprinting && this.stamina > 0 ? 5 : 1; // Determine sprint multiplier
        
            let adjustedSpeed = baseSpeed *  sprintMultiplier; // Adjusted speed based on sprinting
        
            // Consume stamina if sprinting
            if (isSprinting && this.stamina > 0) {
                this.stamina -= this.sprintStaminaConsumptionRate * animationRatio;
            }
        
            // Apply the adjusted speed to the movement direction
            let velocity = this.mesh.physicsImpostor.getLinearVelocity();
            let newVelocity = new Vector3(moveDirection.x * adjustedSpeed, velocity.y, moveDirection.z * adjustedSpeed);
            this.mesh.physicsImpostor.setLinearVelocity(newVelocity);

            // Logging
            const currentTime = performance.now();
            if (currentTime - this.lastLogTime > 300) {
        
            console.log(`Sprint Multiplier: ${sprintMultiplier}`);
            console.log(`Base Speed: ${baseSpeed}`);
            console.log(`Adjusted Speed: ${adjustedSpeed}`);
            console.log(`New Velocity: ${newVelocity}`);
            this.lastLogTime = currentTime; // Update the last log time
            }
        }

    
            
        //create a camera
        private createCamera() {

            // Parameters: name, alpha (rotation), beta (height), radius (distance), target position, scene
            this.camera = new ArcRotateCamera("playerCamera", Math.PI / 4, Math.PI / 4, 10, this.mesh.position, this.scene);
            this.camera.attachControl(this.scene.getEngine().getRenderingCanvas(), true);

            // Lock the camera's target to the player mesh
            this.camera.lockedTarget = this.mesh;
        
            // Optional: adjust camera limits and sensitivities
            this.camera.lowerRadiusLimit = 5;
            this.camera.upperRadiusLimit = 20;

            // Enable pointer lock on canvas click for immersive experience
            this.scene.onPointerDown = () => {
            this.scene.getEngine().enterPointerlock();
        };
        }

        // Aligns the player's rotation with the direction towards the camera's target.

        private alignMeshRotationWithCamera() {

        // Reverse the direction by adjusting the angle
        const yRotation = 2 * Math.PI - this.camera.alpha;
    
        // Calculate target quaternion based on the adjusted angle
        const targetQuaternion = Quaternion.RotationYawPitchRoll(yRotation, 0, 0);
    
        // If the mesh already has a rotation quaternion, interpolate towards the new target
        if (this.mesh.rotationQuaternion) {
            // Slow down the rotation by interpolating between the current and target rotation
            // The factor controls the speed of interpolation, lower values result in slower rotation
            const factor = 0.5; // Adjust this value to make the rotation faster or slower
            Quaternion.SlerpToRef(this.mesh.rotationQuaternion, targetQuaternion, factor, this.mesh.rotationQuaternion);
        } else {
            // If the mesh does not have a rotation quaternion yet, set it directly
            this.mesh.rotationQuaternion = targetQuaternion;
        }
    }
    
    // Inside your Player class to enable CCD
    public enableCCD(ccdMotionThreshold: number, ccdSweptSphereRadius: number): void {
        const ammoBody: any = this.mesh.physicsImpostor.physicsBody;
        if (window.Ammo && ammoBody) {
        ammoBody.setCcdMotionThreshold(ccdMotionThreshold);
        ammoBody.setCcdSweptSphereRadius(ccdSweptSphereRadius);
        } else {
        console.warn("CCD setup failed: Ammo.js is not available or physicsBody is missing.");
    }
    }
            
}


   
       


    
    

