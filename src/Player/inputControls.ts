// inputControls.ts
import { Scene, Vector3, KeyboardEventTypes } from "@babylonjs/core";

export class InputControls {
    private scene: Scene;
    public inputMap: { [key: string]: boolean } = {}; // Tracks which keys are being pressed
    
    constructor(scene: Scene) {
        this.scene = scene;
        this.initializeKeyboardInputs();
        scene.onKeyboardObservable.add((info) => {
            const evt = info.event;
            if (info.type === KeyboardEventTypes.KEYDOWN) {
                this.inputMap[evt.key] = true;
            } else if (info.type === KeyboardEventTypes.KEYUP) {
                this.inputMap[evt.key] = false;
            }
        });

        // Reset input states when the window loses focus to prevent stuck keys
        window.addEventListener('blur', () => {
        this.resetInputMap();
        });

    }
    //Logic for resetting input map.
    private resetInputMap(): void {
        for (let key in this.inputMap) {
            this.inputMap[key] = false;
        }
    }
    //Set the different keys for movement here
    public getInputVector(): Vector3 {
        let inputVector = Vector3.Zero();

        if (this.inputMap["w"] || this.inputMap["ArrowUp"]) {
            inputVector.addInPlace(Vector3.Forward());
        }
        if (this.inputMap["s"] || this.inputMap["ArrowDown"]) {
            inputVector.addInPlace(Vector3.Backward());
        }
        if (this.inputMap["a"] || this.inputMap["ArrowLeft"]) {
            inputVector.addInPlace(Vector3.Left());
        }
        if (this.inputMap["d"] || this.inputMap["ArrowRight"]) {
            inputVector.addInPlace(Vector3.Right());
        }

        return inputVector;
    }

   

    public isSprinting(): boolean {
    return this.inputMap["Shift"] || this.inputMap["shift"]; // Adapt this based on your key mapping
    }

    

    private initializeKeyboardInputs(): void {
        this.scene.onKeyboardObservable.add((info) => {
            const evt = info.event;
            const key = evt.key.toLowerCase(); // Normalize the key to lowercase
            if (info.type === KeyboardEventTypes.KEYDOWN) {
                console.log(`Key down: ${key}`); // Debug log
                this.inputMap[key] = true;
            } else if (info.type === KeyboardEventTypes.KEYUP) {
                console.log(`Key up: ${key}`); // Debug log
                this.inputMap[key] = false;
            }
        });
    }
}
