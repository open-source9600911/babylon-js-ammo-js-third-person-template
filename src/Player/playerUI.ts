import { AdvancedDynamicTexture, Rectangle, Control } from '@babylonjs/gui';

export class PlayerStaminaBar {
    private staminaBar: Rectangle;
    private depletedIndicator: Rectangle;
    private ui: AdvancedDynamicTexture;

    constructor() {
        // Create a full-screen UI for the stamina bar
        this.ui = AdvancedDynamicTexture.CreateFullscreenUI("UI");

        // Create and configure the stamina bar
        this.staminaBar = new Rectangle("staminaBar");
        this.staminaBar.width = "150px";
        this.staminaBar.height = "20px";
        this.staminaBar.cornerRadius = 5;
        this.staminaBar.color = "white";
        this.staminaBar.thickness = 2;
        this.staminaBar.background = "green";
        this.staminaBar.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        this.staminaBar.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;
        this.staminaBar.left = "50px";
        this.staminaBar.top = "50px";
        this.ui.addControl(this.staminaBar); // Add the stamina bar to the UI

        // Create and configure a depleted indicator that appears when stamina is depleted
        this.depletedIndicator = new Rectangle("depletedIndicator");
        this.depletedIndicator.width = "150px";
        this.depletedIndicator.height = "20px";
        this.depletedIndicator.background = "red";
        this.depletedIndicator.isVisible = false; // Initially hidden
        this.depletedIndicator.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        this.depletedIndicator.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;
        this.depletedIndicator.left = "50px";
        this.depletedIndicator.top = "50px";
        this.ui.addControl(this.depletedIndicator); // Add the depleted indicator to the UI
    }

    public update(currentStamina: number): void {
        // Update the width of the stamina bar based on the current stamina
        // Assuming currentStamina is a value between 0 and 1
        const maxWidth = 150; // Max width of the stamina bar in pixels
        this.staminaBar.width = `${maxWidth * currentStamina}px`;

        // If stamina is fully depleted, show the depleted indicator
        this.depletedIndicator.isVisible = currentStamina <= 0;
    }
}



export class JetpackFuelBar {
    private fuelBar: Rectangle;
    private ui: AdvancedDynamicTexture;

    constructor() {
        // Create a full-screen UI for the jetpack fuel bar
        this.ui = AdvancedDynamicTexture.CreateFullscreenUI("UI");

        // Create and configure the jetpack fuel bar
        this.fuelBar = new Rectangle("fuelBar");
        this.fuelBar.width = "150px";
        this.fuelBar.height = "20px";
        this.fuelBar.cornerRadius = 5;
        this.fuelBar.color = "white";
        this.fuelBar.thickness = 2;
        this.fuelBar.background = "blue"; // Blue to distinguish from stamina
        this.fuelBar.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        this.fuelBar.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;
        this.fuelBar.left = "50px";
        this.fuelBar.top = "80px"; // Positioned below the stamina bar
        this.ui.addControl(this.fuelBar); // Add the fuel bar to the UI
    }

    public update(currentFuel: number): void {
        // Assuming currentFuel is a value between 0 and 1
        const maxWidth = 150; // Max width of the fuel bar in pixels
        this.fuelBar.width = `${maxWidth * currentFuel}px`;
    }
}