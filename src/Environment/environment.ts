// Add these imports at the top of your environment.ts file
import { PhysicsImpostor, StandardMaterial, Color3, Scene, MeshBuilder, Vector3, HemisphericLight, Texture } from "@babylonjs/core";

export class Environment {
  private scene: Scene;
 
  constructor(scene: Scene) {
    this.scene = scene;
    this.createGround();
    this.createLight();
    this.createSlope(); // Call the method to create the slope
   
   
  }

  private createGround() {
    const ground = MeshBuilder.CreateGround("ground", { width: 170, height: 150 }, this.scene);
    ground.isPickable = true;

    // Use StandardMaterial for simplicity
    const groundMaterial = new StandardMaterial("groundMaterial", this.scene);

    // Grass texture
    const grassTexture = new Texture("/download.jpeg", this.scene); 
    grassTexture.uScale = 10; // Repeat texture for U coordinates
    grassTexture.vScale = 10; // Repeat texture for V coordinates

    groundMaterial.diffuseTexture = grassTexture;
    groundMaterial.specularColor = new Color3(0, 0, 0); // Remove specular highlight for a more matte look

    ground.material = groundMaterial;

    // Add a physics impostor to the ground
    ground.physicsImpostor = new PhysicsImpostor(ground, PhysicsImpostor.BoxImpostor, { mass: 0, restitution: 0, friction: 80 }, this.scene);

  }


  private createSlope() {
    const slopeLength = 8; // Shorter length of the slope
    const slopeHeight = 5; // Lower height of the slope
    const slopeWidth = 8; // Width of the slope remains the same
    
    // Create a box to use as a slope
    const slope = MeshBuilder.CreateBox("slope", { width: slopeWidth, height: slopeHeight, depth: slopeLength }, this.scene);
    slope.position = new Vector3(0, slopeHeight / 12, 8); // Moving it forward in the scene
    
    // Calculate the rotation to make the slope inclined
    // atan2 is used for a more precise angle calculation
    slope.rotation.z = -Math.atan2(slopeHeight, slopeLength); // Negative to slope upwards as it moves away

    // Material for the slope (optional)
    const slopeMaterial = new StandardMaterial("slopeMaterial", this.scene);
    slopeMaterial.diffuseColor = new Color3(0.8, 0.4, 0.2); // Example: Orange color for visibility
    slope.material = slopeMaterial;

    // Add a physics impostor to the slope
    slope.physicsImpostor = new PhysicsImpostor(slope, PhysicsImpostor.MeshImpostor, { mass: 0, restitution: 0 }, this.scene);
}

  private createLight() {
    new HemisphericLight("light1", new Vector3(1, 10, 0), this.scene);
  }

  

}

 

