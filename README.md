## Vite, Babylon & Ammo Js Third Person Character Controller Template
Choose a self-explaining name for your project.

## Description
A simple third person character controller example, using Vite, Babylon and Ammo js. Feel free to use for personal prjects or for learning. 

## Installation
Open Terminal - cd "folder name you want to install"
git clone https://gitlab.com/open-source9600911/babylon-js-ammo-js-third-person-template.git
npm install
npm run dev

Click on localhost link and you should now see the third person controller template project.

## Authors and acknowledgment
Based on third person controller playground - 

## License
Free to use for personal projects or learning. Made for non commercial use.

## Project status
I have created this template while working on my own game. I do not intend to update this in any way, unless there are bugs found and I am requested by the community to add certain features. I will be keep posting my progress in the babylon js Discord channel, if anyone is interested about the project, or would like updates to this template, feel free to reach out!
